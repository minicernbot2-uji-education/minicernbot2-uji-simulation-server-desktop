using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamera : MonoBehaviour
{
    public float velocityX;
    public float velocityY;
    public float velocityZ;
    public float velocityRotationX;
    public float velocityRotationY;

    //[SerializeField]
    public GameObject robot;

    // Start is called before the first frame update
    void Start()
    {
        velocityX = 0.0f;
        velocityY = 0.0f;
        velocityZ = 0.0f;
        velocityRotationX = 0.0f;
        velocityRotationY = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movement = new Vector3(velocityX * Time.deltaTime, velocityY * Time.deltaTime, velocityZ * Time.deltaTime);
        transform.Translate(movement);
        transform.Rotate(velocityRotationX * Time.deltaTime * 90, velocityRotationY * Time.deltaTime * 90, 0);
    }
}
