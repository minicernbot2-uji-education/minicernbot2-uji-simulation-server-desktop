using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitching : MonoBehaviour
{
    public Camera cam1;
    public Camera cam2;
    public Camera cam3;
    // Start is called before the first frame update
    void Start()
    {
        cam1.enabled = true;
        cam2.enabled = false;
        cam3.enabled = false;
    }

    public void Display1()
    {
        //cam1.enabled = !cam1.enabled;
        //cam2.enabled = !cam2.enabled;
        cam1.enabled = true;
        cam2.enabled = false;
        cam3.enabled = false;
    }
    public void Display2()
    {
        cam1.enabled = false;
        cam2.enabled = true;
        cam3.enabled = false;
    }
    public void Display3()
    {
        cam1.enabled = false;
        cam2.enabled = false;
        cam3.enabled = true;
    }
}