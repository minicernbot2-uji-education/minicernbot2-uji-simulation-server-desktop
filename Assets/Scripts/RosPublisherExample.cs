using UnityEngine;
using Unity.Robotics.ROSTCPConnector;
//using RosMessageTypes.UnityRoboticsDemo;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;

/// <summary>
///
/// </summary>
public class RosPublisherExample : MonoBehaviour
{
    ROSConnection ros;
    public string topicName = "/cmd_vel";

    public ControlRobot controlRobot;

    // Publish the cube's position and rotation every N seconds
    public float publishMessageFrequency = 0.001f;

    // Used to determine how much time has elapsed since the last message was published
    private float timeElapsed;

    void Start()
    {
        // start the ROS connection
        ros = ROSConnection.GetOrCreateInstance();
        ros.RegisterPublisher<RosMessageTypes.Geometry.TwistMsg> (topicName);
    }

    private void Update()
    {
        timeElapsed += Time.deltaTime;

        if (timeElapsed > publishMessageFrequency)
        {
            RosMessageTypes.Geometry.TwistMsg RobotVelocities = new RosMessageTypes.Geometry.TwistMsg();
            RobotVelocities.linear.x = controlRobot.velocityX;
            RobotVelocities.linear.y = -controlRobot.velocityY;
            RobotVelocities.angular.z = controlRobot.velocityRotation;
            // Finally send the message to server_endpoint.py running in ROS
            ros.Publish(topicName, RobotVelocities);

            timeElapsed = 0;
        }
    }
}
