using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlRobot : MonoBehaviour
{
    public float velocityX; //Front
    public float velocityY; //Left
    public float velocityRotation;
    public bool isStatic = false;

    //[SerializeField]
    public GameObject robot;

    public Wheel1ControlScript wheel1Script;
    public Wheel1ControlScript wheel2Script;
    public Wheel1ControlScript wheel3Script;
    public Wheel1ControlScript wheel4Script;
     

// Start is called before the first frame update
void Start()
    {
        velocityX = 0.0f;
        velocityY = 0.0f;
        velocityRotation = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if(isStatic == false)
        {
            Vector3 movement = new Vector3(velocityY * Time.deltaTime, velocityX * Time.deltaTime, 0);
            transform.Translate(movement);
            transform.Rotate(0, 0, velocityRotation * Time.deltaTime * -90);
        }

        wheel1Script.velocityWheel = -5.0f * (velocityX + velocityY + 1.0f * velocityRotation);
        wheel2Script.velocityWheel = -5.0f * (velocityX - velocityY - 1.0f * velocityRotation);
        wheel3Script.velocityWheel = -5.0f * (velocityX + velocityY - 1.0f * velocityRotation);
        wheel4Script.velocityWheel = -5.0f * (velocityX - velocityY + 1.0f * velocityRotation);
    }
}
