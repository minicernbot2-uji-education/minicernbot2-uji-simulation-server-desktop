using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wheel1ControlScript : MonoBehaviour
{
    public float velocityWheel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(velocityWheel * Time.deltaTime * 90, 0, 0);
    }
}
