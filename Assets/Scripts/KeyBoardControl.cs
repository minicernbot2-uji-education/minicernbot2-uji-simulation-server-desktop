using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyBoardControl : MonoBehaviour
{
    public float horizontalInput;
    public float verticalInput;
    public float horizontal2Input;
    public float v;
    public float h2;
    public float h1;

    public ControlRobot controlRobot;

    public float velocity;
    // Start is called before the first frame update
    void Start()
    {
        horizontalInput = 0;
        verticalInput = 0;
        horizontal2Input = 0;

        velocity = 0.2f;
    }

    // Update is called once per frame
    void Update()
    {
        h1 = Input.GetAxis("Horizontal");
        h2 = Input.GetAxis("Horizontal2");
        v = Input.GetAxis("Vertical");

        //Joystick dead point solution
        if (h1 > 0.1f || h1 < -0.1f)
        {
            horizontalInput = h1;
        }
        else
        {
            horizontalInput = 0f;
        }
        if (h2 > 0.1f || h2 < -0.1f)
        {
            horizontal2Input = h2;
        }
        else
        {
            horizontal2Input = 0f;
        }
        if (v > 0.1f || v < -0.1f)
        {
            verticalInput = v;
        }
        else
        {
            verticalInput = 0f;
        }

        controlRobot.velocityX = -verticalInput * velocity;
        controlRobot.velocityY = horizontalInput * velocity;
        controlRobot.velocityRotation = -horizontal2Input * velocity*6;

        //Velocity control*****
        if (Input.GetKeyDown(KeyCode.R))
        {
            velocity = velocity + 0.1f;
        }
        else if (Input.GetKeyDown(KeyCode.F))
        {
            velocity = velocity - 0.1f;
        }
    }
}
