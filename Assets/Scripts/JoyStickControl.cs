using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoyStickControl : MonoBehaviour
{
    public float horizontalInput;
    public float verticalInput;
    public float horizontal2Input;

    public Joystick joyStickMovement;
    public Joystick joyStickRotation;

    public ControlRobot controlRobot;

    public float velocity;
    // Start is called before the first frame update
    void Start()
    {
        horizontalInput = 0;
        verticalInput = 0;
        horizontal2Input = 0;

        velocity = 1.0f;
    }

    // Update is called once per frame
    void Update()
    {
        horizontalInput = joyStickMovement.Horizontal;
        horizontal2Input = joyStickRotation.Horizontal;
        verticalInput = joyStickMovement.Vertical;
        controlRobot.velocityY = horizontalInput * velocity;
        controlRobot.velocityX = verticalInput * velocity;
        controlRobot.velocityRotation = -horizontal2Input * velocity;
    }

    public void ChangeSpeed(float newSpeed)
    {
        velocity = newSpeed;
    }
}
