using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraKeyBoardController : MonoBehaviour
{
    [Header("Look Sensitivity")]
    public float sensX;
    public float sensY;

    [Header("Clamping")]
    public float minY;
    public float maxY;

    [Header("Spectator")]
    public float spectatorMoveSpeed;

    private float rotX;
    private float rotY;

    private bool isSpectator = true;

    void Start()
    {
        // lock the cursor to the middle of the screen
        Cursor.lockState = CursorLockMode.Locked;
    }

    void LateUpdate()
    {
        // get mouse movement inputs
        if (Input.GetKey(KeyCode.Mouse1))
        {
            rotX += Input.GetAxis("Mouse X") * sensX;
            rotY += Input.GetAxis("Mouse Y") * sensY;
        }
        
        // clamp the vertical rotation
        rotY = Mathf.Clamp(rotY, minY, maxY);

        // are we spectating?
        if (isSpectator)
        {
            // rotate the cam vertically
            transform.rotation = Quaternion.Euler(-rotY, rotX, 0);

            //movement
            float x = Input.GetAxis("CameraHorizontal");
            float z = Input.GetAxis("CameraVertical");
            float y = 0;

            if (Input.GetKey(KeyCode.O))
                y = 1;
            else if (Input.GetKey(KeyCode.U))
                y = -1;

            Vector3 dir = transform.right * x + transform.up * y + transform.forward * z;
            transform.position += dir * spectatorMoveSpeed * Time.deltaTime;
        }
        else
        {

        }
    }
}
