using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class RealRobotHTTPControlScript : MonoBehaviour
{
    public string robotServerIP = "194.12.179.143";
    public string robotServerPort = "8081";
    public bool transmiting = true;
    private string command;
    public ControlRobot controlRobot;

    void Start()
    {
        //_ = 
        StartCoroutine(sendURLRequestRobotMovement());
    }

    void Update()
    {
        //yield return new WaitForSeconds(0.001F);
    }

    IEnumerator sendURLRequestRobotMovement()
    {
        while(transmiting)
        {
            //-/SETVELOCITY?X=0.1&Y=0.1&AZ=0.1
            command = "/SETVELOCITY?X=" + (controlRobot.velocityY).ToString() + "&Y=" + (-controlRobot.velocityX).ToString() + "&Z=" + (controlRobot.velocityRotation).ToString();
            //command = "/SETVELOCITY?X=0.1&Y=0.1&AZ=0.1";
            command = command.Replace(",", ".");
            Debug.Log(command);
            
            Debug.Log("Sending....");
            UnityWebRequest www = UnityWebRequest.Get("" + "http://" + this.robotServerIP + ":" + this.robotServerPort + command);
            Debug.Log("Sending....");
            Debug.Log(www);
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                //// Show results as text
                //Debug.Log(www.downloadHandler.text);

                //// Or retrieve results as binary data
                //byte[] results = www.downloadHandler.data;

                //string jsonString = System.Text.Encoding.ASCII.GetString(results);
                //this.debugString = jsonString;
                //position = JsonUtility.FromJson<GetPositionCommandReplyJSON>(jsonString);
                //this.setRobotPosition.setPosition(position);
                
            }
            yield return new WaitForSeconds(0.1f);
        }
    }


    //public void turnLeft(float _velocity)
    //{
    //    StartCoroutine(sendURLRequestRobotMovement("/turnleft"));
    //}
}
