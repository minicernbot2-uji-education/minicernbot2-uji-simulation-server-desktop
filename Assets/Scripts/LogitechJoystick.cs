using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogitechJoystick : MonoBehaviour
{
    public float a;
    public float b;
    public float c;
    public float d;
    public float f;
    public float g;
    public float h;
    public float y;
    public float j;
    public float k;
    public float q;
    public float w;
    public float p;
    public float u;
    // Start is called before the first frame update
    void Start()
    {
        a = 0;
        b = 0;
        c = 0;
        d = 0;
        f = 0;
        g = 0;
        h = 0;
        y = 0;
        j = 0;
        k = 0;
        q = 0;
        w = 0;
        u = 0;
        p = 0;
    }

    // Update is called once per frame
    void Update()
    {
        a = Input.GetAxis("Horizontal");
        b = Input.GetAxis("Vertical");
        c = Input.GetAxis("Fire1");
        d = Input.GetAxis("Fire2");
        f = Input.GetAxis("Fire3");
        g = Input.GetAxis("Jump");
        h = Input.GetAxis("Mouse X");
        y = Input.GetAxis("Mouse Y");
        j = Input.GetAxis("Mouse ScrollWheel");
        k = Input.GetAxis("Horizontal2");
        q = Input.GetAxis("CameraHorizontal");
        w = Input.GetAxis("CameraVertical");
        u = Input.GetAxis("Submit");
        p = Input.GetAxis("Cancel");
    }
}
