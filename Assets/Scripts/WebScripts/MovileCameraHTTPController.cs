using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using System;

public class MovileCameraHTTPController : WebServerScript
{
    public ControlCamera controller;
    public float DEFAULT_SPEED = 1.0f;
    String robot_command_param = "";
    public float robot_command_param_float = 1.0f;
    float[] robot_setvelocity_command_params = new float[4] { 1, 1, 1, 1 }; //velocities: x, y, z, angular
    public void OnServerInitialized()
    {
        if (this.controller == null)
        {
            this.controller = GetComponent<ControlCamera>();
        }
    }

    private ControlCamera getController()
    {
        if (this.controller == null)
        {
            this.controller = GetComponent<ControlCamera>();
            robot_command_param_float = DEFAULT_SPEED;
            for (int i = 0; i < this.robot_setvelocity_command_params.Length; i++)
            {
                this.robot_setvelocity_command_params[i] = DEFAULT_SPEED;
            }
        }
        return this.controller;
    }

    public override void processHTMLRequestResource(Socket socket, String url)
    {
        //base.processHTMLRequestResource(socket, url);
        Debug.Log(this.GetType() + ":URL=" + url);
        //if (url.ToUpper().StartsWith("/GETPOSITION"))
        //{
        //    //this.currentRobotCmd = RobotCommand.MOVE_DOWN_CMD;
        //    Vector3 position = this.controller.getPosition();
        //    Vector3 rotation = this.controller.getRotation();
        //    //Vector3 positionRobotFormat = new Vector3(position.x, position.z, -position.y);
        //    Vector3 positionRobotFormat = new Vector3(position.x, position.y, position.z);
        //    GetPositionCommandReplyJSON positionJsonObject = new GetPositionCommandReplyJSON(positionRobotFormat, rotation);

        //    string json = JsonUtility.ToJson(positionJsonObject);

        //    String headerStr = getHTTP_Header(CODE_OK, "application/json", json.Length);
        //    SendPacket(socket, headerStr);
        //    SendPacket(socket, json);
        //}
        //else
        //if (url.ToUpper().StartsWith("/STOP"))
        //{
        //    this.getController().stop();
        //}
        //else if (url.ToUpper().StartsWith("/FORWARD"))
        //{
        //    this.getController().forward(robot_setvelocity_command_params[0]);
        //}
        //else if (url.ToUpper().StartsWith("/BACKWARD"))
        //{
        //    this.getController().backward(robot_setvelocity_command_params[0]);
        //}
        //else if (url.ToUpper().StartsWith("/LEFT"))
        //{
        //    this.getController().moveLeft(robot_setvelocity_command_params[2]);
        //}
        //else if (url.ToUpper().StartsWith("/RIGHT"))
        //{
        //    this.getController().moveRight(robot_setvelocity_command_params[2]);
        //}
        //else if (url.ToUpper().StartsWith("/TURNRIGHT"))
        //{
        //    this.getController().turnRight(robot_setvelocity_command_params[3]);
        //}
        //else if (url.ToUpper().StartsWith("/TURNLEFT"))
        //{
        //    this.getController().turnLeft(robot_setvelocity_command_params[3]);
        //}

        //Nos centramos en la x ************

        if (url.ToUpper().StartsWith("/SETVELOCITY"))
        {
            Debug.Log(":/SETVELOCITY command request");
            String command = "/SETVELOCITY?";
            int paramsPositionInString = command.Length;
            string parameters = url.Substring(paramsPositionInString);
            HTTPTools.SetRequestParameters(parameters);
            //string p = url.SearchParameters["myParameter"];
            if (HTTPTools.HasKey("X")) //Velocity X axis
            {
                Debug.Log(":HTTPTools.HasKey(X)");
                string KeyValue = HTTPTools.GetValue("X");
                float value = (float)HTTPTools.stringToDouble(KeyValue);
                Debug.Log(":X value =" + value);
                robot_setvelocity_command_params[0] = value;
                this.getController().velocityX = value;
            }
            if (HTTPTools.HasKey("Y")) //Velocity Y axis
            {
                string KeyValue = HTTPTools.GetValue("Y");
                float value = (float)HTTPTools.stringToDouble(KeyValue);
                robot_setvelocity_command_params[1] = value;
                this.getController().velocityY = value;
            }
            if (HTTPTools.HasKey("Z")) //Velocity Z axis
            {
                string KeyValue = HTTPTools.GetValue("Z");
                float value = (float)HTTPTools.stringToDouble(KeyValue);
                robot_setvelocity_command_params[2] = value;
                this.getController().velocityZ = value;
            }
            if (HTTPTools.HasKey("RX")) //Angular velocity vertical axis
            {
                string KeyValue = HTTPTools.GetValue("RX");
                float value = (float)HTTPTools.stringToDouble(KeyValue);
                robot_setvelocity_command_params[3] = value;
                this.getController().velocityRotationX = value;
            }
            if (HTTPTools.HasKey("RY")) //Angular velocity vertical axis
            {
                string KeyValue = HTTPTools.GetValue("RY");
                float value = (float)HTTPTools.stringToDouble(KeyValue);
                robot_setvelocity_command_params[3] = value;
                this.getController().velocityRotationY = value;
            }
        }
        String headerStr = HTTPTools.getHTTP_Header(HTTPTools.CODE_OK, "text/html", fileStr.Length);
        HTTPTools.SendPacket(socket, headerStr);
        HTTPTools.SendPacket(socket, fileStr);
    }
}
