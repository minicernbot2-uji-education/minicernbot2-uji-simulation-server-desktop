﻿#define DEBUG_ENABLED

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using CERNRoboticGUI.Communication;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine.UI;
using System.Text;

public class HTTPTools
{
    public const int CODE_OK = 200;
    public const int CODE_BADREQUEST = 400;
    public const int CODE_FORBIDDEN = 403;
    public const int CODE_NOTFOUND = 404;
    public const int CODE_INTERNALSERVERERROR = 500;
    public const int CODE_NOTIMPLEMENTED = 501;

    public static Dictionary<string, string> parameters = new Dictionary<string, string>();

    public static bool HasKey(string key)
    {
        return parameters.ContainsKey(key.ToUpper());
    }

    // This can be called from Start(), but not earlier
    public static string GetValue(string key)
    {
        return parameters[key.ToUpper()];
    }

    public static void SetRequestParameters(string parametersString)
    {
        //clean previous dictionary
        parameters = new Dictionary<string, string>();
        char[] parameterDelimiters = new char[] { '?', '&' };
        string[] parametersStrings = parametersString.ToUpper().Split(parameterDelimiters, System.StringSplitOptions.RemoveEmptyEntries);


        char[] keyValueDelimiters = new char[] { '=' };
        for (int i = 0; i < parametersStrings.Length; ++i)
        {
            string[] keyValue = parametersStrings[i].Split(keyValueDelimiters, System.StringSplitOptions.None);

            if (keyValue.Length >= 2)
            {
                parameters.Add(WWW.UnEscapeURL(keyValue[0]), WWW.UnEscapeURL(keyValue[1]));
            }
            else if (keyValue.Length == 1)
            {
                parameters.Add(WWW.UnEscapeURL(keyValue[0]), "");
            }
        }
    }


    public static double stringToDouble(String value)
    {
        if (value.Contains("."))
        {
            value = value.Replace(".", ",");
        }
        double result = 0.0;
        try
        {
            result = Convert.ToDouble(value);
            Console.WriteLine("Converted '{0}' to {1}.", value, result);
        }
        catch (FormatException)
        {
            Console.WriteLine("Unable to convert '{0}' to a Double.", value);
            if (value.Contains("."))
            {
                value.Replace(".", ",");
                return stringToDouble(value);
            }
            else if (value.Contains(","))
            {
                value.Replace(",", ".");
                return stringToDouble(value);
            }
        }
        catch (OverflowException)
        {
            Console.WriteLine("'{0}' is outside the range of a Double.", value);
        }
        return result;
    }


    public static float RadianToDegree(float angle)
    {
        return angle * (180.0f / (float)Math.PI);
    }


    /// <summary>
    /// Receives a String through TCP
    /// </summary>
    /// <returns></returns>
    public static String ReceiveStringLinePacket(Socket socket)
    {
        if ((socket!=null)&&(socket.Connected))
        {
            byte[] http_get_command_buffer_part = new byte[1];
            int ret_val = 0;
            String http_get_command_String = "";
            Boolean endOfLineFound = false;
            while (!endOfLineFound)
            {
                try
                {
                    ret_val = socket.Receive(http_get_command_buffer_part, 1, SocketFlags.None);
                    if (ret_val == 1)
                    {
                        String dataString = Encoding.ASCII.GetString(http_get_command_buffer_part, 0, ret_val);
                        if ((dataString != "\r\n") && (dataString != "\n") && (dataString != "\r"))
                        {
                            http_get_command_String = http_get_command_String + dataString;
                        }
                        else
                        {
                            endOfLineFound = true;
                        }
                    }
                    else if (ret_val == 0)
                    {
                        endOfLineFound = true;
                    }
                }
                catch (Exception e)
                {
                    //throw e;
                    return null;
                }
            }

            return http_get_command_String;
        }
        else
        {
            return null;
        }
    }

    public static String ReceiveString(Socket socket)
    {
        String http_get_command_String = "";
        if (socket.Connected)
        {
            byte[] http_get_command_buffer_part = new byte[1];
            int ret_val = 0;

            Boolean endOfDataFound = false;
            while (!endOfDataFound)
            {
                try
                {
                    ret_val = socket.Receive(http_get_command_buffer_part, 1, SocketFlags.None);
                    if (ret_val == 1)
                    {
                        String dataString = Encoding.ASCII.GetString(http_get_command_buffer_part, 0, ret_val);
                        http_get_command_String = http_get_command_String + dataString;
                    }
                    else if (ret_val == 0)
                    {
                        endOfDataFound = true;
                    }
                }
                catch (Exception e)
                {
                    Debug.Log("ReceiveJSONString:ERROR:" + e);
                    //throw e;
                    //return null;
                }
            }
        }
        return http_get_command_String;
    }

    public static String ReceiveJSONFileRequest(Socket socket)
    {
        String http_get_command_String = "";
        if (socket.Connected)
        {
            byte[] http_get_command_buffer_part = new byte[1024];
            int ret_val = 0;

            Boolean endOfDataFound = false;
            while (!endOfDataFound)
            {
                try
                {
                    ret_val = socket.Receive(http_get_command_buffer_part, 1024, SocketFlags.None);
                    if (ret_val == 1024)
                    {
                        String dataString = Encoding.ASCII.GetString(http_get_command_buffer_part, 0, ret_val);
                        http_get_command_String = http_get_command_String + dataString;
                    }
                    else
                    {
                        String dataString = Encoding.ASCII.GetString(http_get_command_buffer_part, 0, ret_val);
                        http_get_command_String = http_get_command_String + dataString;
                        endOfDataFound = true;
                    }
                }
                catch (Exception e)
                {
                    Debug.Log("ReceiveJSONString:ERROR:" + e);
                    //throw e;
                    //return null;
                }
            }
        }
        return http_get_command_String;
    }


    /// <summary>
    /// Sends a byte[] through TCP
    /// </summary>
    /// <returns></returns>
    public static void SendPacket(Socket socket, byte[] byData)
    {
        if ((socket != null) && (socket.Connected))
        {
            //byte[] http_get_command_buffer = new byte[1024];
            //int ret_val = 0;
            try
            {
                //byte[] byData = System.Text.Encoding.ASCII.GetBytes(str);
                socket.Send(byData);
            }
            catch (Exception e)
            {
                //throw e;
                //return null;
            }
        }
        else
        {
            //return null;
        }
    }


    /// <summary>
    /// Sends a String through TCP
    /// </summary>
    /// <returns></returns>
    public static void SendPacket(Socket socket, String str)
    {
        if ((socket != null) && (socket.Connected))
        {
            byte[] http_get_command_buffer = new byte[1024];
            int ret_val = 0;
            try
            {
                byte[] byData = System.Text.Encoding.ASCII.GetBytes(str);
                socket.Send(byData);
            }
            catch (Exception e)
            {
                //throw e;
                //return null;
            }
        }
        else
        {
            //return null;
        }
    }

    public static String getHTTP_HeaderStatus(int headerStatusCode)
    {
        String result = "";
        switch (headerStatusCode)
        {
            case CODE_OK:
                result = "200 OK";
                break;
            case CODE_BADREQUEST:
                result = "400 Bad Request";
                break;
            case CODE_FORBIDDEN:
                result = "403 Forbidden";
                break;
            case CODE_NOTFOUND:
                result = "404 Not Found";
                break;
            case CODE_INTERNALSERVERERROR:
                result = "500 Internal Server Error";
                break;
            case CODE_NOTIMPLEMENTED:
                result = "501 Not Implemented";
                break;
        }
        return ("HTTP/1.0 " + result);
    }

    public static String getHTTP_HeaderContentLength(int headerFileLength)
    {
        return "Content-Length: " + headerFileLength + "\r\n";
    }

    public static String getHTTP_HeaderContentType(String headerContentType)
    {
        return "Content-Type: " + headerContentType + "\r\n";
    }


    public static String getHTTP_Header(int headerStatusCode, String headerContentType, int headerFileLength)
    {
        String result = getHTTP_HeaderStatus(headerStatusCode) +
                   "\r\n" +
                   getHTTP_HeaderContentLength(headerFileLength) +
                   getHTTP_HeaderContentType(headerContentType) +
                   "\r\n";

        return result;
    }

    public static String readResourceTextFile(String fileName)
    {
        String fileStr = "";

        try
        {
            // fileStr = File.ReadAllText(@"index.html", Encoding.UTF8);
            TextAsset txt = (TextAsset)Resources.Load(fileName, typeof(TextAsset));
            fileStr = txt.text;
        }
        catch (IOException e)
        {
            //this.debugString = "ERROR: readResourceTextFile"+fileName+e;
            throw e;
        }

        return fileStr;
    }
}


