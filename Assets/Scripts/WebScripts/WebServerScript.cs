﻿//
// File: WebServerScript.cs
// Author: Raúl Marín Prades
// History:  1/ 7/2019 Creation
//          24/ 4/2019 Prepare to be able to inherit from this class 


//#define DEBUG_ENABLED

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using CERNRoboticGUI.Communication;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine.UI;
using System.Text;

public class WebServerScript : MonoBehaviour
{
    String debugString = "WebServerScript:DEBUG";
    //public Text statusText;

    //public GameObject TIMWagonGameObject;
    //public GameObject BLMArmGameObject;
    //private Canvas canvas;
    //private TIMBLMKinovaARMCERNGUIController timBLMKinovaARMCERNGUIController;
    //private BLM_TargetList_Scripts blm_TargetList_Scripts;

    //TCPServer variables

    public NetworkStream stream;
    ThreadStart threadStart;
    Thread thread;

    public int TCPServerPort = 8000;
    private TcpListener server = null;
    public Boolean IsConnected = false;

    protected static bool TimestampSyncronized { get; private set; }
    private static long _TimestampDifference;

    //Thread control
    public Boolean exit = false;

    public Socket socket;

    /*
    //Robot Commands
    public enum RobotCommand
    {
        NONE,
        TIM_APPROACH_CMD,
        BLMARM_J0V_CMD,
        BLMARM_J1V_CMD,
        BLMARM_J2V_CMD,
        BLMARM_J3V_CMD,
        BLMARM_J4V_CMD,
        BLMARM_J5V_CMD,
        BLMARM_J6V_CMD,
        BLMARM_J7V_CMD,
        BLMARM_J8V_CMD
    }

    private RobotCommand currentRobotCmd = RobotCommand.NONE;
    String robot_command_param = "";
    float robot_command_param_float = 0.0f;
    */

    public const int CODE_OK = 200;
    public const int CODE_BADREQUEST = 400;
    public const int CODE_FORBIDDEN = 403;
    public const int CODE_NOTFOUND = 404;
    public const int CODE_INTERNALSERVERERROR = 500;
    public const int CODE_NOTIMPLEMENTED = 501;

    public String htmlResourceName = "index";
    public String fileStr = "";
    //private String fileStrPositions = "";

    //private Boolean updateBLMPositions = false;
    //private Boolean updateTIMPosition = false;

    //private Vector3[] positionBLMs;
    //private Vector3 positionBLMToApproach;
    //private Vector3 positionTIMBLMWagon;

    // Use this for initialization
    void Start()
    {
        //GameObject parent = this.GetComponent<GameObject>();
        GameObject parent = transform.gameObject;
        //if (parent != null)
        //{
            //this.debugString = "WebServerScript:Start:Start:" + parent.name;
        //    Debug.Log("WEBSERVERSCRIPT:PARENT=" + parent.name);
        //}
        this.fileStr = readResourceTextFile(htmlResourceName);
        Debug.Log(this.GetType()+":INIT:PARENT=" +parent.name+"PORT="+this.TCPServerPort+":index.html=" + this.fileStr);
        this.threadStart = new ThreadStart(launchTCPServerVersion2);
        this.thread = new Thread(threadStart);
        this.thread.Start();
        this.debugString = "WebServerScript:Start:End";
        initialize();
    }

    public void OnDestroy()
    {
        this.exit = true;
        if (this.socket != null)
        {
            this.socket.Close();
        }
    }




    void OnApplicationQuit()
    {
        Debug.Log(this.GetType() + ":" + this.TCPServerPort + ":" + "Application ending after " + Time.time + " seconds");
        this.exit = true;
        if (this.socket != null)
        {
            this.socket.Close();
        }

        if (this.thread != null)
        {
            if (this.thread.IsAlive)
            {
                if (this.server != null) this.server.Stop();
                this.server = null;
                if (this.thread != null) this.thread.Abort();
                Debug.Log(this.thread.IsAlive); //true (must be false)
                this.thread = null;
            }
            else
            {
                //Debug.Log("Dead duck on Thanksgiving table");
                this.server = null;
                this.thread = null;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    private float RadianToDegree(float angle)
    {
        return angle * (180.0f / (float)Math.PI);
    }

    //FixedUpdate is called after the physics calculations have been accomplished
    private void FixedUpdate()
    {

    }

    /// <summary>
    /// Receives a String through TCP
    /// </summary>
    /// <returns></returns>
    public String ReceiveStringLinePacket(Socket socket)
    {
        if (IsConnected)
        {
            byte[] http_get_command_buffer_part = new byte[1];
            int ret_val = 0;
            String http_get_command_String = "";
            Boolean endOfLineFound = false;
            while (!endOfLineFound)
            {
                try
                {
                    ret_val = socket.Receive(http_get_command_buffer_part, 1, SocketFlags.None);
                    if (ret_val == 1)
                    {
                        String dataString = Encoding.ASCII.GetString(http_get_command_buffer_part, 0, ret_val);
                        if ((dataString != "\r\n") && (dataString != "\n") && (dataString != "\r"))
                        {
                            http_get_command_String = http_get_command_String + dataString;
                        }
                        else
                        {
                            endOfLineFound = true;
                        }
                    }
                    else if (ret_val == 0)
                    {
                        endOfLineFound = true;
                    }
                }
                catch (Exception e)
                {
                    //throw e;
                    //return null;
                }
            }


            return http_get_command_String;
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Sends a byte[] through TCP
    /// </summary>
    /// <returns></returns>
    public void SendPacket(Socket socket, byte[] byData)
    {
        if (IsConnected)
        {
            //byte[] http_get_command_buffer = new byte[1024];
            //int ret_val = 0;
            try
            {
                //byte[] byData = System.Text.Encoding.ASCII.GetBytes(str);
                socket.Send(byData);
            }
            catch (Exception e)
            {
                //throw e;
                //return null;
            }
        }
        else
        {
            //return null;
        }
    }


    /// <summary>
    /// Sends a String through TCP
    /// </summary>
    /// <returns></returns>
    public void SendPacket(Socket socket, String str)
    {
        if (IsConnected)
        {
            byte[] http_get_command_buffer = new byte[1024];
            int ret_val = 0;
            try
            {
                byte[] byData = System.Text.Encoding.ASCII.GetBytes(str);
                socket.Send(byData);
            }
            catch (Exception e)
            {
                //throw e;
                //return null;
            }
        }
        else
        {
            //return null;
        }
    }

    private String getHTTP_HeaderStatus(int headerStatusCode)
    {
        String result = "";
        switch (headerStatusCode)
        {
            case CODE_OK:
                result = "200 OK";
                break;
            case CODE_BADREQUEST:
                result = "400 Bad Request";
                break;
            case CODE_FORBIDDEN:
                result = "403 Forbidden";
                break;
            case CODE_NOTFOUND:
                result = "404 Not Found";
                break;
            case CODE_INTERNALSERVERERROR:
                result = "500 Internal Server Error";
                break;
            case CODE_NOTIMPLEMENTED:
                result = "501 Not Implemented";
                break;
        }
        return ("HTTP/1.0 " + result);
    }

    private String getHTTP_HeaderContentLength(int headerFileLength)
    {
        return "Content-Length: " + headerFileLength + "\r\n";
    }

    private String getHTTP_HeaderContentType(String headerContentType)
    {
        return "Content-Type: " + headerContentType + "\r\n";
    }

    public String getHTTP_Header(int headerStatusCode, String headerContentType, int headerFileLength)
    {
        String result = getHTTP_HeaderStatus(headerStatusCode) +
                   "\r\n" +
                   getHTTP_HeaderContentLength(headerFileLength) +
                   getHTTP_HeaderContentType(headerContentType) +
                   "\r\n";

        return result;
    }

    public String readResourceTextFile(String fileName)
    {
        String fileStr = "";

        try
        {
            // fileStr = File.ReadAllText(@"index.html", Encoding.UTF8);
            TextAsset txt = (TextAsset)Resources.Load(fileName, typeof(TextAsset));
            fileStr = txt.text;
        }
        catch (IOException e)
        {
            this.debugString = "ERROR: readResourceTextFile"+fileName+e;
        }

        return fileStr;
    }

    private void handle_connection_socket(IAsyncResult result)  //the parameter is a delegate, used to communicate between threads
    {
        this.debugString = "WebServerScript:handle_connection_socket:Start";
        this.socket = server.EndAcceptSocket(result);  //creates the TcpClient
        this.IsConnected = true;
        socket.NoDelay = true;

        try
        {
            // Loop to receive all the data sent by the client. 
            exit = false;

            while ((!exit))
            {
                try
                {
                    if (socket != null)
                    {
                        String http_get_command_String = ReceiveStringLinePacket(socket);
                        this.debugString = "WebServerScript:handle_connection_socket:"+ http_get_command_String;
                        Debug.Log(this.debugString);
                        if ((http_get_command_String != null) && (!http_get_command_String.Equals("")))
                        {
                            string[] get_commmand_tokens = http_get_command_String.Split(' ');
                            String url = get_commmand_tokens[1];
                            this.debugString = "WebServerScript:handle_connection_socket:url=" + url;
                            string[] url_tokens = url.Split('/');

                            String robot = "";

                            //if (url_tokens.Length > 0) {
                            //    robot = url_tokens[1];
                            //}
                            //String robot_command = url_tokens[2];
                            //String robot_command_param = url_tokens[3];
                            //if (url.Equals("/")||(url.ToUpper().Contains("index")))
                            processHTMLRequestResource(socket, url);
                            //String headerStr = getHTTP_Header(CODE_OK, "text/html", this.fileStr.Length);
                            //SendPacket(socket, headerStr);
                            //SendPacket(socket, this.fileStr);
                            /*
                            if(true)
                            {
                                //this.debugString = "WebServerScript:handle_connection_socket:url.Equals(/)";
                                this.debugString = fileStr;
                                String headerStr = getHTTP_Header(CODE_OK, "text/html", fileStr.Length);
                                SendPacket(socket, headerStr);
                                SendPacket(socket, fileStr);
                            }
                            */
                            /*
                            else if (robot.ToUpper().Equals("TIM"))
                            {
                                String robot_command = url_tokens[2];
                                if (robot_command.ToUpper().Equals("APPROACH"))
                                {
                                    this.currentRobotCmd = RobotCommand.TIM_APPROACH_CMD;
                                    robot_command_param = url_tokens[3].ToUpper(); //The parameter is the name of the target (e.g. BLM0001)
                                    //robot_command_param_float = float.Parse(robot_command_param);
                                    String headerStr = getHTTP_Header(CODE_OK, "text/html", fileStr.Length);
                                    SendPacket(socket, headerStr);
                                    SendPacket(socket, fileStr);
                                }
                            }
                            else if (robot.ToUpper().Equals("BLMARM"))
                            {
                                String robot_command = url_tokens[2];
                                if (robot_command.ToUpper().Equals("J0V"))
                                {
                                    this.currentRobotCmd = RobotCommand.BLMARM_J0V_CMD;
                                    robot_command_param = url_tokens[3];
                                    robot_command_param_float = float.Parse(robot_command_param);
                                    String headerStr = getHTTP_Header(CODE_OK, "text/html", fileStr.Length);
                                    SendPacket(socket, headerStr);
                                    SendPacket(socket, fileStr);
                                }
                                else if (robot_command.ToUpper().Equals("J1V"))
                                {
                                    this.currentRobotCmd = RobotCommand.BLMARM_J1V_CMD;
                                    robot_command_param = url_tokens[3];
                                    robot_command_param_float = float.Parse(robot_command_param);
                                    String headerStr = getHTTP_Header(CODE_OK, "text/html", fileStr.Length);
                                    SendPacket(socket, headerStr);
                                    SendPacket(socket, fileStr);
                                }
                                else if (robot_command.ToUpper().Equals("J2V")) //Linear Actuator
                                {
                                    this.currentRobotCmd = RobotCommand.BLMARM_J2V_CMD;
                                    robot_command_param = url_tokens[3];
                                    robot_command_param_float = float.Parse(robot_command_param);
                                    String headerStr = getHTTP_Header(CODE_OK, "text/html", fileStr.Length);
                                    SendPacket(socket, headerStr);
                                    SendPacket(socket, fileStr);
                                }
                                else if (robot_command.ToUpper().Equals("J3V")) //Base
                                {
                                    this.currentRobotCmd = RobotCommand.BLMARM_J3V_CMD;
                                    robot_command_param = url_tokens[3];
                                    robot_command_param_float = float.Parse(robot_command_param);
                                    String headerStr = getHTTP_Header(CODE_OK, "text/html", fileStr.Length);
                                    SendPacket(socket, headerStr);
                                    SendPacket(socket, fileStr);
                                }
                                else if (robot_command.ToUpper().Equals("J4V")) //Shoulder
                                {
                                    this.currentRobotCmd = RobotCommand.BLMARM_J4V_CMD;
                                    robot_command_param = url_tokens[3];
                                    robot_command_param_float = float.Parse(robot_command_param);
                                    String headerStr = getHTTP_Header(CODE_OK, "text/html", fileStr.Length);
                                    SendPacket(socket, headerStr);
                                    SendPacket(socket, fileStr);
                                }
                                else if (robot_command.ToUpper().Equals("J5V")) //Elbow
                                {
                                    this.currentRobotCmd = RobotCommand.BLMARM_J5V_CMD;
                                    robot_command_param = url_tokens[3];
                                    robot_command_param_float = float.Parse(robot_command_param);
                                    String headerStr = getHTTP_Header(CODE_OK, "text/html", fileStr.Length);
                                    SendPacket(socket, headerStr);
                                    SendPacket(socket, fileStr);
                                }
                                else if (robot_command.ToUpper().Equals("J6V")) //Elbow
                                {
                                    this.currentRobotCmd = RobotCommand.BLMARM_J6V_CMD;
                                    robot_command_param = url_tokens[3];
                                    robot_command_param_float = float.Parse(robot_command_param);
                                    String headerStr = getHTTP_Header(CODE_OK, "text/html", fileStr.Length);
                                    SendPacket(socket, headerStr);
                                    SendPacket(socket, fileStr);
                                }
                                else if (robot_command.ToUpper().Equals("J7V")) //Wrist
                                {
                                    this.currentRobotCmd = RobotCommand.BLMARM_J7V_CMD;
                                    robot_command_param = url_tokens[3];
                                    robot_command_param_float = float.Parse(robot_command_param);
                                    String headerStr = getHTTP_Header(CODE_OK, "text/html", fileStr.Length);
                                    SendPacket(socket, headerStr);
                                    SendPacket(socket, fileStr);
                                }
                                else if (robot_command.ToUpper().Equals("J8V")) //Wrist
                                {
                                    this.currentRobotCmd = RobotCommand.BLMARM_J8V_CMD;
                                    robot_command_param = url_tokens[3];
                                    robot_command_param_float = float.Parse(robot_command_param);
                                    String headerStr = getHTTP_Header(CODE_OK, "text/html", fileStr.Length);
                                    SendPacket(socket, headerStr);
                                    SendPacket(socket, fileStr);
                                }
                                else if (robot_command.ToUpper().Equals("STOP"))
                                {
                                    this.currentRobotCmd = RobotCommand.BLMARM_J1V_CMD;
                                    robot_command_param_float = 0.0f;
                                    String headerStr = getHTTP_Header(CODE_OK, "text/html", fileStr.Length);
                                    SendPacket(socket, headerStr);
                                    SendPacket(socket, fileStr);
                                }
                            }
                            */
                            /*
                            else
                            {
                                this.debugString = fileStr;
                                String headerStr = getHTTP_Header(CODE_OK, "text/html", fileStr.Length);
                                SendPacket(socket, headerStr);
                                SendPacket(socket, fileStr);
                            }
                            */
                        }
                        //SendPacket(socket, http_get_command_String);
                        exit = true;
                        this.IsConnected = false;
                        
                    }
                    else
                    {
                        exit = true;
                        this.IsConnected = false;
                    }
                }
                catch (Exception e)
                {
                    exit = true;
                    if (socket != null)
                    {
                        socket.Close();
                    }
                    this.IsConnected = false;
                }
            }
            //client.Close();
            waitForConnection();
            //launchTCPServerVersion2();
        }
        catch (Exception e)
        {
            if (socket != null)
            {
                socket.Close();
            }
            this.IsConnected = false;
        }
    }


    public virtual void initialize()
    {

    }

    public virtual void processHTMLRequestResource(Socket socket, String url)
    {
        /*
        if (url.ToUpper().Equals("/CLOSE"))
        {
            String headerStr = getHTTP_Header(CODE_OK, "text/html", fileStr.Length);
            SendPacket(socket, headerStr);
            SendPacket(socket, fileStr);
        }
        */
    }
    

    private void waitForConnection()
    {
        try
        {
            //server.BeginAcceptTcpClient(handle_connection, server);
            server.BeginAcceptSocket(handle_connection_socket, server);
            this.debugString = "WebServerScript:waitForConnection():" + this.TCPServerPort;
            //Debug.Log("WebServerScript:waitForConnection():" + this.TCPServerPort);
        }
        catch (SocketException e)
        {
            Debug.Log("SocketException: {0}" + e);
            this.debugString = "ERROR: WebServerScript:waitForConnection():" + this.TCPServerPort+ "SocketException: {0}" + e;
        }
    }

    private void launchTCPServerVersion2()
    {
        this.debugString = "WebServerScript:launchTCPServerVersion2:Start";
        //Debug.Log(this.debugString);
        //TcpListener server = null;
        TcpClient client = null;

        try
        {
            Int32 port = this.TCPServerPort;
            //IPAddress localAddr = IPAddress.Parse("127.0.0.1");
            server = new TcpListener(IPAddress.Any, port);
            // Start listening for client requests.
            //this.updateBLMsPositionsValues();
            server.Start();
            waitForConnection();
        }
        catch (SocketException e)
        {
            //Comented*************************************************************************
            //Debug.Log(this.transform.parent.name + ":" + this.GetType() + ":" + "PORT=" + this.TCPServerPort + "SocketException: {0}" + e);
        }
        finally
        {
            // Stop listening for new clients.
            //server.Stop();
        }
        this.debugString = "WebServerScript:launchTCPServerVersion2:End";
    }

#if DEBUG_ENABLED
    public void OnGUI()
    {
        GUI.Label(new Rect(400, 100, 1000, 1000), debugString);
    }
#endif
    //void OnControllerColliderHit(ControllerColliderHit hit)
    //{
    //    Rigidbody body = hit.collider.attachedRigidbody;
    //    if (body != null && !body.isKinematic)
    //        body.velocity += hit.controller.velocity;
    //}




}


