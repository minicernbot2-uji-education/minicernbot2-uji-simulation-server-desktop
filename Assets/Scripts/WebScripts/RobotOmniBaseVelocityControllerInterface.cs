using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface RobotOmniBaseVelocityControllerInterface
{
    void setVelocityX(float _velocityX);
    void setVelocityY(float _velocityY);
    //void setVelocityZ(float _velocityZ);
    void setAngularVelocityY(float _velocityY);

    void changeCurrentVelocity(float velocity); //Change the velocity of the current movement (e.g. forward)

    void setVelocities(float _velocityX, float _velocityY, float _velocityZ, float _angularVelocityY);
    //float getMinVelocity();
    //float getMaxVelocity();
    //Vector3 getPosition();

    void stop();
    void forward(float _velocity);
    void backward(float _velocity);
    void moveLeft(float _velocity);
    void moveRight(float _velocity);
    void turnLeft(float _velocity);
    void turnRight(float _velocity);
}
