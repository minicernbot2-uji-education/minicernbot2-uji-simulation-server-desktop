﻿/*
 * FILE NAME: CameraHTTPController.cs 
 * DESCRIPTION: Script in Unity that can be attached to a camera and offers its realtime image via HTTP
 * AUTHOR: RAUL MARIN PRADES, 20/4/2020, Jaume I University of Castellon
 * HISTORY:     Creation, 20/4/2020, Raúl Marín
 *              Adapted to CERN scenario, 1/6/2020, Raul Marin
 *              Adapted to H2020-ElPeacetolero Project, 11/2/2021, Raul Marin, Jaume I University
 * 				Added MJPEG functionality, 6/6/2021, Raul Marin, Jaume I University
 * 				Added IFG, QUALITY and WIDTH parameters to the MJPEG functionality, Raul Marin, Jaume I University
 */

//#define DEBUG_ENABLED

//using CERNRoboticGUI.Communication;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;
//using CERNRoboticGUI.Communication.Packets;

public class CameraHTTPController : MonoBehaviour
{
    String debugString = "CameraHTTPController:DEBUG";

    //TCPServer variables
    NetworkStream stream;
    ThreadStart threadStart;
    Thread thread;

    Rect maximizedRect = new Rect(0.0f, 0.0f, 1.0f, 1.0f);

    public int TCPServerPort = 5050;
    private TcpListener server = null;
    private Boolean IsConnected = false;
    private Boolean IsCaptureEnable = false;
    private Boolean IsWebServerCall = false;

    //private CERNRoboticGUI.Communication.Packets.JpegImagePacket capturedJpegPacket;
    private byte[] capturedJpegImage = null;

    protected static bool TimestampSyncronized { get; private set; }

    public bool serverActive;
    private static long _TimestampDifference;
    Camera _camera;
    public int resWidth;
    public int resHeight;
    private static int DEFAULT_RES_WIDTH = 360;
    private static int DEFAULT_RES_HEIGHT = 240;
    public int compressionQuality = 75; //From 0 to 100 quality of the compression in JPEG
    public int CAMERA_IPG; //Gap in time between every packet that is sent by the camera, in milliseconds
    private int CAMERA_IPG_DEFAULT = 250;
    public bool DEBUG_ENABLED = true;
    public int DEBUG_X = 100;
    public int DEBUG_Y = 100;

    public bool mpegServiceExit = false;
    public int MJPEG_IPG = 200; //Milliseconds to wait before sending the next mjpeg frame
    Socket mjpegSocket = null;

    const int CODE_OK = 200;
    const int CODE_BADREQUEST = 400;
    const int CODE_FORBIDDEN = 403;
    const int CODE_NOTFOUND = 404;
    const int CODE_INTERNALSERVERERROR = 500;
    const int CODE_NOTIMPLEMENTED = 501;


    Boolean exit = false;

    public uint metaData01, metaData02;  //Data that can be adept to the header of every packet sent as image. It can be used to transmit metadata information
    public bool isStarted;

    public static long TimestampDifference
    {
        get
        {
            return _TimestampDifference;
        }
        set
        {
            _TimestampDifference = value;
            TimestampSyncronized = true;
        }
    }

    //Use this for initialization
    void Start()
    {
#if DEBUG_ENABLED
        this.debugString = "CameraHTTPController: Start Begin";
        DebugConsole.Log("CameraHTTPController: Start Begin");
#endif
        this._camera = GetComponent<Camera>();
        if (resWidth <= 0)
        {
            resWidth = DEFAULT_RES_WIDTH;
        }
        if (resHeight <= 0)
        {
            resHeight = DEFAULT_RES_HEIGHT;
        }
        if (CAMERA_IPG <= 0)
        {
            CAMERA_IPG = CAMERA_IPG_DEFAULT;
        }

        if (serverActive)
        {
            this.activateServer(this.TCPServerPort);
        }
        this.isStarted = true;

        //print ("StartThread");
        /*
        this.threadStart = new ThreadStart(launchTCPServerVersion2);
        this.thread = new Thread(threadStart);
        this.thread.Start();
        TimestampSyncronized = false;
        */
#if DEBUG_ENABLED
        //this.debugString = "CameraHTTPController: Start End";
#endif
    }

    public void activateServer(int _port)
    {
        this.debugString = "activateServer:ServerActive="+serverActive;
        this.TCPServerPort = _port;
        this.threadStart = new ThreadStart(launchTCPServerVersion2);
        this.thread = new Thread(threadStart);
        this.thread.Start();
        TimestampSyncronized = false;
        serverActive = true;
    }

    public void Close()
    {
        try
        {
            this.exit = true;
            if (this.thread != null)
            {
                if (this.thread.IsAlive)
                {
                    if (this.server != null) this.server.Stop();
                    this.server = null;
                    if (this.thread != null)
                    {
                        this.thread.Abort();
                    }
                    Debug.Log(this.thread.IsAlive); //true (must be false)
                    this.thread = null;
                    this.threadStart = null;
                }
                else
                {
                    //Debug.Log("Dead duck on Thanksgiving table");
                    this.server = null;
                    this.thread = null;
                    this.threadStart = null;
                }
            }
        }catch(Exception e)
        {
            Debug.Log("ERROR:Close: CameraCERNGUIController:" + e);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnApplicationQuit()
    {
        Close();
    }

    //FixedUpdate is called after the physics calculations have been accomplished
    private void FixedUpdate()
    {
        if (IsWebServerCall)
        {
            //this.capturedJpegPacket = getPacketFromCurrentCamera();
            this.capturedJpegImage = this.getCompressedJPEGImageBytesFromCurrentCamera();
            this.IsWebServerCall = false;
        }
    }

    public byte[] getCapturedJpegImage()
    {
        this.IsWebServerCall = true;
        //System.Threading.Thread.Sleep(CAMERA_IPG);
        while (this.IsWebServerCall)
        {
            if (this.capturedJpegImage == null)
            {
                this.IsWebServerCall = true;
            }
        }
        if (this.capturedJpegImage != null)
        {
            return this.capturedJpegImage;
        }
        return null;
    }


    //TO BE TESTED LHZ ALGORITTHM: 
    //https://forum.unity.com/threads/lzf-compression-and-decompression-for-unity.152579/?_ga=2.220363472.917923334.1613054165-208845463.1518111956
    private byte[] getCompressedJPEGImageBytesFromCurrentCamera()
    {
        try
        {
            Rect rectAux = this._camera.rect;
            
            if ((rectAux.width != 1.0) || (rectAux.height != 1.0)){
                this._camera.rect = maximizedRect;
            }
            RenderTexture rt = new RenderTexture(this.resWidth, this.resHeight, 24);
            _camera.targetTexture = rt;
            Texture2D screenShot = new Texture2D(this.resWidth, this.resHeight, TextureFormat.RGB24, false);
            //Texture2D screenShot = new Texture2D(this.resWidth, this.resHeight, TextureFormat.Alpha8, false);
            //Texture2D screenShot = new Texture2D(this.resWidth, this.resHeight, TextureFormat.Alpha8, false);
            _camera.Render();
            RenderTexture.active = rt;
            //screenShot.Resize(this.resWidth, this.resHeight, TextureFormat.Alpha8, false);
            screenShot.ReadPixels(new Rect(0, 0, this.resWidth, this.resHeight), 0, 0);
            
            _camera.targetTexture = null;
            RenderTexture.active = null; // JC: added to avoid errors
            Destroy(rt);
            //byte[] bytes = screenShot.EncodeToPNG();
            byte[] bytes = screenShot.EncodeToJPG(this.compressionQuality);
            //byte[] bytes = screenShot.EncodeToPNG();
            Destroy(screenShot);
            return bytes;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR:CameraHTTPController.cs:getComressedJPEGImageBytesFromCurrentCamera:" + e);
            return null;
        }
    }

    /*
    private CERNRoboticGUI.Communication.Packets.JpegImagePacket getPacketFromCurrentCamera()
    {
        try
        {
            CERNRoboticGUI.Communication.Packets.JpegImagePacket packet = new CERNRoboticGUI.Communication.Packets.JpegImagePacket();
            byte[] bytes = getCompressedJPEGImageBytesFromCurrentCamera();
            packet.Bytes = bytes;
            return packet;
        }
        catch (Exception e)
        {
            return null;
        }
    }
    */


    /// <summary>
    /// Receives a String through TCP
    /// </summary>
    /// <returns></returns>
    public String ReceiveStringLinePacket(Socket socket)
    {
        if (IsConnected)
        {
            byte[] http_get_command_buffer_part = new byte[1];
            int ret_val = 0;
            String http_get_command_String = "";
            Boolean endOfLineFound = false;
            while (!endOfLineFound)
            {
                try
                {
                    ret_val = socket.Receive(http_get_command_buffer_part, 1, SocketFlags.None);
                    if (ret_val == 1)
                    {
                        String dataString = Encoding.ASCII.GetString(http_get_command_buffer_part, 0, ret_val);
                        if ((dataString != "\r\n") && (dataString != "\n") && (dataString != "\r"))
                        {
                            http_get_command_String = http_get_command_String + dataString;
                        }
                        else
                        {
                            endOfLineFound = true;
                        }
                    }
                    else if (ret_val == 0)
                    {
                        endOfLineFound = true;
                    }
                }
                catch (Exception e)
                {
                    //throw e;
                    //return null;
                }
            }


            return http_get_command_String;
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Sends a byte[] through TCP
    /// </summary>
    /// <returns></returns>
    public bool SendPacket(Socket socket, byte[] byData)
    {
        if (IsConnected)
        {
            //byte[] http_get_command_buffer = new byte[1024];
            //int ret_val = 0;
            try
            {
                //byte[] byData = System.Text.Encoding.ASCII.GetBytes(str);
                socket.Send(byData);
            }
            catch (Exception e)
            {
                //throw e;
                return false;
            }
        }
        else
        {
            return false;
        }
        return true;
    }


    /// <summary>
    /// Sends a String through TCP
    /// </summary>
    /// <returns></returns>
    public bool SendPacket(Socket socket, String str)
    {
        if (IsConnected)
        {
            byte[] http_get_command_buffer = new byte[1024];
            int ret_val = 0;
            try
            {
                byte[] byData = System.Text.Encoding.ASCII.GetBytes(str);
                socket.Send(byData);
            }
            catch (Exception e)
            {
                return false;
            }
        }
        else
        {
            return false;
        }
        return true;
    }

    private String getHTTP_HeaderStatus(int headerStatusCode)
    {
        String result = "";
        switch (headerStatusCode)
        {
            case CODE_OK:
                result = "200 OK";
                break;
            case CODE_BADREQUEST:
                result = "400 Bad Request";
                break;
            case CODE_FORBIDDEN:
                result = "403 Forbidden";
                break;
            case CODE_NOTFOUND:
                result = "404 Not Found";
                break;
            case CODE_INTERNALSERVERERROR:
                result = "500 Internal Server Error";
                break;
            case CODE_NOTIMPLEMENTED:
                result = "501 Not Implemented";
                break;
        }
        return ("HTTP/1.0 " + result);
    }

    private String getHTTP_HeaderContentLength(int headerFileLength)
    {
        return "Content-Length: " + headerFileLength + "\r\n";
    }

    private String getHTTP_HeaderContentType(String headerContentType)
    {
        return "Content-Type: " + headerContentType + "\r\n";
    }


    private String getHTTP_Header(int headerStatusCode, String headerContentType, int headerFileLength)
    {
        String result = getHTTP_HeaderStatus(headerStatusCode) +
                   "\r\n" +
                   getHTTP_HeaderContentLength(headerFileLength) +
                   getHTTP_HeaderContentType(headerContentType) +
                   "\r\n";

        return result;
    }

    private void handle_connection_socket(IAsyncResult result)  //the parameter is a delegate, used to communicate between threads
    {
        this.debugString = "handle_connection_socket: BEGIN";
        Socket socket = server.EndAcceptSocket(result);  //creates the TcpClient
                                                         // DebugConsole.Log("Connection");
        this.debugString = "CameraHTTPController:handle_connection_socket:IsConnected";
        this.IsConnected = true;
        socket.NoDelay = true;

        try
        {
            // Loop to receive all the data sent by the client. 
            exit = false;

            while ((!exit))
            {
                try
                {
                    if (socket != null)
                    {
                        String http_get_command_String = ReceiveStringLinePacket(socket);
                        //Debug.Log(http_get_command_String);
                        //this.debugString = http_get_command_String;
                        if ((http_get_command_String != null) && (!http_get_command_String.Equals("")))
                        {
                            string[] get_commmand_tokens = http_get_command_String.Split(' ');
                            String url = get_commmand_tokens[1];

                            string[] url_tokens = url.Split('/');

                            String robot = "";


                            if (url_tokens.Length > 0)
                            {
                                robot = url_tokens[1];
                            }
                            //String robot_command = url_tokens[2];
                            //String robot_command_param = url_tokens[3];
                            if (url.ToUpper().Equals("/") || url.ToUpper().Equals("/CAM") || url.ToUpper().Equals("/CAMERACOLOR.JPG") || url.ToUpper().Equals("/CAMERA.JPG") || url.ToUpper().Contains("INDEX") )
                            {
                                //String robot_command_param = url_tokens[3];
                                //robot_command_param_float = float.Parse(robot_command_param);
                                byte[] cameraImage = getCapturedJpegImage();
                                int fileLength = cameraImage.Length;
                                String headerStr = getHTTP_Header(CODE_OK, "image/jpeg", fileLength);
                                SendPacket(socket, headerStr);
                                SendPacket(socket, cameraImage);
                            }
                            else if (url.ToUpper().Equals("/CAMERACOLOR1280X960.JPG"))
                            {
                                //String robot_command_param = url_tokens[3];
                                //robot_command_param_float = float.Parse(robot_command_param);
                                this.resWidth = 1280;
                                this.resHeight = 960;

                                byte[] cameraImage = getCapturedJpegImage();
                                int fileLength = cameraImage.Length;
                                String headerStr = getHTTP_Header(CODE_OK, "image/jpeg", fileLength);
                                SendPacket(socket, headerStr);
                                SendPacket(socket, cameraImage);
                            }
                            else if (url.ToUpper().Equals("/CAMERACOLOR640X480.JPG"))
                            {
                                //String robot_command_param = url_tokens[3];
                                //robot_command_param_float = float.Parse(robot_command_param);
                                this.resWidth = 640;
                                this.resHeight = 480;

                                byte[] cameraImage = getCapturedJpegImage();
                                int fileLength = cameraImage.Length;
                                String headerStr = getHTTP_Header(CODE_OK, "image/jpeg", fileLength);
                                SendPacket(socket, headerStr);
                                SendPacket(socket, cameraImage);
                            }
                            else if (url.ToUpper().Equals("/CAMERACOLOR320X240.JPG"))
                            {
                                //String robot_command_param = url_tokens[3];
                                //robot_command_param_float = float.Parse(robot_command_param);
                                this.resWidth = 320;
                                this.resHeight = 240;

                                byte[] cameraImage = getCapturedJpegImage();
                                int fileLength = cameraImage.Length;
                                String headerStr = getHTTP_Header(CODE_OK, "image/jpeg", fileLength);
                                SendPacket(socket, headerStr);
                                SendPacket(socket, cameraImage);
                            }
                            else if (url.ToUpper().Equals("/CAMERACOLOR160X120.JPG"))
                            {
                                //String robot_command_param = url_tokens[3];
                                //robot_command_param_float = float.Parse(robot_command_param);
                                this.resWidth = 160;
                                this.resHeight = 120;

                                byte[] cameraImage = getCapturedJpegImage();
                                int fileLength = cameraImage.Length;
                                String headerStr = getHTTP_Header(CODE_OK, "image/jpeg", fileLength);
                                SendPacket(socket, headerStr);
                                SendPacket(socket, cameraImage);
                            }
                            else if (url.ToUpper().Equals("/QUALITY100"))
                            {
                                this.compressionQuality = 100;

                                byte[] cameraImage = getCapturedJpegImage();
                                int fileLength = cameraImage.Length;
                                String headerStr = getHTTP_Header(CODE_OK, "image/jpeg", fileLength);
                                SendPacket(socket, headerStr);
                                SendPacket(socket, cameraImage);
                            }
                            else if (url.ToUpper().Equals("/QUALITY10"))
                            {
                                this.compressionQuality = 10;

                                byte[] cameraImage = getCapturedJpegImage();
                                int fileLength = cameraImage.Length;
                                String headerStr = getHTTP_Header(CODE_OK, "image/jpeg", fileLength);
                                SendPacket(socket, headerStr);
                                SendPacket(socket, cameraImage);
                            }
                            else if (url.ToUpper().Equals("/QUALITY50"))
                            {
                                this.compressionQuality = 50;

                                byte[] cameraImage = getCapturedJpegImage();
                                int fileLength = cameraImage.Length;
                                String headerStr = getHTTP_Header(CODE_OK, "image/jpeg", fileLength);
                                SendPacket(socket, headerStr);
                                SendPacket(socket, cameraImage);
                            }
                            else if (url.ToUpper().StartsWith("/CAMERA.MJPEG")|| url.ToUpper().StartsWith("/CAMERA.MJPG"))
                            {
                                this.mjpegSocket = socket;
                                //StartCoroutine(handleMPEGRequest());
                                handleMPEGRequest(url.ToUpper());
                            }
                            else if (url.ToUpper().StartsWith("/SETPARAMS"))
                            {
                                {
                                    //Debug.Log("/SETPARAMS");
                                    String command = "/SETPARAMS";
                                    int paramsPositionInString = command.Length;
                                    //Debug.Log(":paramsPositionInString="+ paramsPositionInString);

                                    string parameters = url.Substring(paramsPositionInString);
                                    //Debug.Log(":parameters=" + parameters);
                                    HTTPTools.SetRequestParameters(parameters);
                                    //string p = url.SearchParameters["myParameter"];
                                    if (HTTPTools.HasKey("QUALITY")) //Image QUALITY
                                    {
                                        //Debug.Log(":HTTPTools.HasKey(QUALITY)");

                                        string KeyValue = HTTPTools.GetValue("QUALITY");
                                        float value = (float)HTTPTools.stringToDouble(KeyValue);
                                        //Debug.Log(":QUALITY value =" + value);
                                        if ((value > 0) && (value <= 100))
                                        {
                                            this.compressionQuality = (int)value;
                                        }
                                    }
                                    if (HTTPTools.HasKey("WIDTH")) //Image WIDTH Y axis
                                    {
                                        //Debug.Log(":HTTPTools.HasKey(WIDTH)");
                                        string KeyValue = HTTPTools.GetValue("WIDTH");
                                        float value = (float)HTTPTools.stringToDouble(KeyValue);
                                        int valueInt = (int)value;
                                        if ((valueInt > 0) && (valueInt < 10000)){
                                            this.resWidth = valueInt;
                                            this.resHeight = (this.resWidth * 480)/640;
                                        }
                                    }
                                    if (HTTPTools.HasKey("IFG")) //Inter Frame Gap in milliseconds when using MJPEG
                                    {
                                        //Debug.Log(":HTTPTools.HasKey(WIDTH)");
                                        string KeyValue = HTTPTools.GetValue("IFG");
                                        float value = (float)HTTPTools.stringToDouble(KeyValue);
                                        int valueInt = (int)value;
                                        if ((valueInt > 0) && (valueInt < 10000))
                                        {
                                            this.MJPEG_IPG = valueInt;
                                        }
                                    }
                                }
                                byte[] cameraImage = getCapturedJpegImage();
                                int fileLength = cameraImage.Length;
                                String headerStr = getHTTP_Header(CODE_OK, "image/jpeg", fileLength);
                                SendPacket(socket, headerStr);
                                SendPacket(socket, cameraImage);
                            }
                        }
                        //SendPacket(socket, http_get_command_String);
                        exit = true;
                        this.IsConnected = false;

                    }
                    else
                    {
                        exit = true;
                        this.IsConnected = false;
                    }
                }
                catch (Exception e)
                {
                    exit = true;
                    if (socket != null)
                    {
                        socket.Close();
                    }
                    this.IsConnected = false;
                }
            }
            //client.Close();
            waitForConnection();
            //launchTCPServerVersion2();
        }
        catch (Exception e)
        {
            if (socket != null)
            {
                socket.Close();
            }
            this.IsConnected = false;
        }
    }

    public void handleMPEGRequest(string url)
    {
        Debug.Log("handleMPEGRequest:START");
        String mpegHeaderStr = "HTTP/1.0 200 OK\r\n" +
                        "Server: RobotSimulationServer\r\n" +
                        "Connection: close\r\n" +
                        "Max-Age: 0\r\n" +
                        "Expires: 0\r\n" +
                        "Cache-Control: no-cache, private\r\n" +
                        "Pragma: no-cache\r\n" +
                        "Content-Type: multipart/x-mixed-replace; " +
                        "boundary=--BoundaryString\r\n\r\n";
        this.mpegServiceExit = !SendPacket(this.mjpegSocket, mpegHeaderStr);

        //yield return new WaitForSeconds(0.200f);

        {
            Debug.Log("url=" + url);
            int positionParameters = url.IndexOf("?");
            if (positionParameters > 0)
            {
                string parameters = url.Substring(positionParameters);
                Debug.Log("Parameters=" + parameters);
                HTTPTools.SetRequestParameters(parameters);
                //string p = url.SearchParameters["myParameter"];
                if (HTTPTools.HasKey("QUALITY")) //Image QUALITY
                {
                    //Debug.Log(":HTTPTools.HasKey(QUALITY)");

                    string KeyValue = HTTPTools.GetValue("QUALITY");
                    float value = (float)HTTPTools.stringToDouble(KeyValue);
                    //Debug.Log(":QUALITY value =" + value);
                    if ((value > 0) && (value <= 100))
                    {
                        this.compressionQuality = (int)value;
                    }
                }
                if (HTTPTools.HasKey("WIDTH")) //Image WIDTH Y axis
                {
                    //Debug.Log(":HTTPTools.HasKey(WIDTH)");
                    string KeyValue = HTTPTools.GetValue("WIDTH");
                    float value = (float)HTTPTools.stringToDouble(KeyValue);
                    int valueInt = (int)value;
                    if ((valueInt > 0) && (valueInt < 10000))
                    {
                        this.resWidth = valueInt;
                        this.resHeight = (this.resWidth * 480) / 640;
                    }
                }
                if (HTTPTools.HasKey("IFG")) //Inter Frame Gap in milliseconds when using MJPEG
                {
                    //Debug.Log(":HTTPTools.HasKey(WIDTH)");
                    string KeyValue = HTTPTools.GetValue("IFG");
                    float value = (float)HTTPTools.stringToDouble(KeyValue);
                    int valueInt = (int)value;
                    if ((valueInt > 10) && (valueInt < 10000))
                    {
                        this.MJPEG_IPG = valueInt;
                    }
                }
            }
        }

        while (!this.mpegServiceExit)
        {
            if (this.mjpegSocket.Connected)
            {
                byte[] cameraImage = getCapturedJpegImage();
                int fileLength = cameraImage.Length;
                String mpegFrameHeaderStr = "--BoundaryString\r\n" +
                                        "Content-type: image/jpg\r\n" +
                                        "Content-Length: " +
                                        fileLength +
                                        "\r\n\r\n";
                this.mpegServiceExit = !SendPacket(this.mjpegSocket, mpegFrameHeaderStr);
                this.mpegServiceExit = !SendPacket(this.mjpegSocket, cameraImage);
                this.mpegServiceExit = !SendPacket(this.mjpegSocket, "\r\n\r\n");
                float seconds = ((float)this.MJPEG_IPG / 1000.0f);
                Thread.Sleep(this.MJPEG_IPG);
                //yield return new WaitForSeconds(this.MJPEG_IPG);
            }
            else
            {
                this.mpegServiceExit = true;
                this.mjpegSocket.Close();
            }
        }
        Debug.Log("handleMPEGRequest:END");
    }

    /*
    IEnumerator handleMPEGRequest()
    {
            Debug.Log("handleMPEGRequest:START");
            String mpegHeaderStr = "HTTP/1.0 200 OK\r\n" +
                            "Server: YourServerName\r\n" +
                            "Connection: close\r\n" +
                            "Max-Age: 0\r\n" +
                            "Expires: 0\r\n" +
                            "Cache-Control: no-cache, private\r\n" +
                            "Pragma: no-cache\r\n" +
                            "Content-Type: multipart/x-mixed-replace; " +
                            "boundary=--BoundaryString\r\n\r\n";
            this.mpegServiceExit = !SendPacket(this.mjpegSocket, mpegHeaderStr);

            yield return new WaitForSeconds(0.200f);

            while (!this.mpegServiceExit)
            {
                if (this.mjpegSocket.Connected)
                {
                    byte[] cameraImage = getCapturedJpegImage();
                    int fileLength = cameraImage.Length;
                    String mpegFrameHeaderStr = "--BoundaryString\r\n" +
                                            "Content-type: image/jpg\r\n" +
                                            "Content-Length: " +
                                            fileLength +
                                            "\r\n\r\n";
                    this.mpegServiceExit = !SendPacket(this.mjpegSocket, mpegFrameHeaderStr);
                    this.mpegServiceExit = !SendPacket(this.mjpegSocket, cameraImage);
                    this.mpegServiceExit = !SendPacket(this.mjpegSocket, "\r\n\r\n");
                    float seconds = ((float)this.MJPEG_IPG / 1000.0f);
                    yield return new WaitForSeconds(this.MJPEG_IPG);
                }
                else
                {
                    this.mpegServiceExit = true;
                this.mjpegSocket.Close();
                }
            }
            Debug.Log("handleMPEGRequest:END");
    }
    */


    private void waitForConnection()
    {
        try
        {
            //server.BeginAcceptTcpClient(handle_connection, server);
            if (server != null ) server.BeginAcceptSocket(handle_connection_socket, server);
            debugString = "CameraHTTPController:waitForConnection";
            Debug.Log("Waiting for a connection... ");
        }
        catch (SocketException e)
        {
            Debug.Log("SocketException: {0}" + e);
        }
    }

    private void launchTCPServerVersion2()
    {
        //TcpListener server = null;
        TcpClient client = null;

        try
        {
            Int32 port = this.TCPServerPort;
            //IPAddress localAddr = IPAddress.Parse("127.0.0.1");
            server = new TcpListener(IPAddress.Any, port);
            // Start listening for client requests.
            server.Start();
            waitForConnection();
            debugString = "CameraHTTPController:launchTCPServerVersion2:ServerStart Port:"+port;
        }
        catch (SocketException e)
        {
            Debug.Log("SocketException: {0}" + e+":PORT="+this.TCPServerPort);
#if DEBUG_ENABLED
            this.debugString = "CameraHTTPController: launchTCPServerVersion2"+"SocketException: {0}" + e;
#endif
        }
        finally
        {
            // Stop listening for new clients.
            //server.Stop();
        }
    }


    // This example requires the System and System.Net namespaces.
    void launchTCPServerVersion3()
    {
        if (!HttpListener.IsSupported)
        {
            Console.WriteLine("Windows XP SP2 or Server 2003 is required to use the HttpListener class.");
            return;
        }
        // URI prefixes are required,
        // for example "http://contoso.com:8080/index/".
        //if (prefixes == null || prefixes.Length == 0)
        //    throw new ArgumentException("prefixes");

        // Create a listener.
        HttpListener listener = new HttpListener();
        // Add the prefixes.
        //foreach (string s in prefixes)
        //{
        //    listener.Prefixes.Add(s);
        //}
        listener.Start();
        //Console.WriteLine("Listening...");
        // Note: The GetContext method blocks while waiting for a request. 
        HttpListenerContext context = listener.GetContext();
        HttpListenerRequest request = context.Request;
        // Obtain a response object.
        HttpListenerResponse response = context.Response;
        // Construct a response.
        string responseString = "<HTML><BODY> Hello world!</BODY></HTML>";
        byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
        // Get a response stream and write the response to it.
        response.ContentLength64 = buffer.Length;
        System.IO.Stream output = response.OutputStream;
        output.Write(buffer, 0, buffer.Length);
        // You must close the output stream.
        output.Close();
        listener.Stop();
    }

    /*
    void launchTCPServerVersion1()
    {
        server = null;
        try
        {
            // Set the TcpListener on port 13000.
            Int32 port = 12345;
            //IPAddress localAddr = IPAddress.Parse("192.168.0.242");

            //server = new TcpListener(port);
            //IPAddress localAddr = IPAddress.Parse("127.0.0.1");

            // TcpListener server = new TcpListener(port);
            server = new TcpListener(IPAddress.Any, port);
            //server = new TcpListener(IPAddress.Any, port);
            //IPAddress iplocal = new IPAddress()
            //server = new TcpListener(IPAddress.None, port);

            // Start listening for client requests.
            server.Start();

            // Buffer for reading data
            Byte[] bytes = new Byte[256];
            String data = null;

            // Enter the listening loop.
            while (true)
            {
                //Thread.Sleep(10);

                //Debug.Log("Waiting for a connection... ");

                // Perform a blocking call to accept requests.
                // You could also use server.AcceptSocket() here.
                //Socket socket = server.AcceptSocket();
                client = server.AcceptTcpClient();
                if (client != null)
                {
                    Debug.Log("Connected!");
                    //isConnection=true;
                    //client.Close();
                    //break;
                }
                else
                {
                    data = null;

                    // Get a stream object for reading and writing
                    stream = client.GetStream();
                    StreamWriter swriter = new StreamWriter(stream);

                    int i;

                    // Loop to receive all the data sent by the client.
                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        //msg1 = System.Text.Encoding.ASCII.GetBytes(prevdata);

                        // Send back a response.
                        //stream.Write(msg1, 0, msg1.Length);
                        // Translate data bytes to a ASCII string.
                        data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                        //Debug.Log("Received:"+ data+"data");


                        //Debug.Log("Sent:"+ data);
                        // Process the data sent by the client.
                        //bool isTrue = false;
                        switch (data)
                        {
                            case "dx":
                                byte[] msg = System.Text.Encoding.ASCII.GetBytes(data + "ok");
                                stream.Write(msg, 0, msg.Length);
                                //stream.Flush();
                                this.applyVelocityX = 0.1f;
                                break;
                        }
                    }
                    // Shutdown and end connection
                    client.Close();
                }
            }
        }
        catch (SocketException e)
        {
            Debug.Log("SocketException:" + e);
        }
        finally
        {
            // Stop listening for new clients.
            server.Stop();
        }

        //yield return null;


    }
    */
//#if DEBUG_ENABLED
    //public void OnGUI()
    //{
    //    if (DEBUG_ENABLED)
    //    {
    //        GUI.Label(new Rect(DEBUG_X, DEBUG_Y, 1000, 1000), debugString);
    //    }
    //}
//#endif
}
